#!/bin/bash

# Fetch instance names
INSTANCE_NAMES=$(gcloud compute instance-groups list-instances instance-group-manager --zone us-central1-a --format="value(NAME)")

# Check if there are any instances
if [ -z "$INSTANCE_NAMES" ]; then
  echo "No instances found."
  exit 1
fi

DOCKER_IMAGE_NAME=$DOCKER_MAIN_REPO:$DOCKER_MAIN_IMAGE-$TAG_VERSION

# Project ID
GCP_PROJECT="capstone-project-424112"

# Loop through each instance
for INSTANCE_NAME in $INSTANCE_NAMES; do

  # Copy gcp-owner.json to the instance
  echo "Copying gcp-owner.json to $INSTANCE_NAME..."
  gcloud compute scp gcp-owner.json abalabanovic7818ri@"$INSTANCE_NAME": --project="$GCP_PROJECT" --zone=us-central1-a

  # SSH into the instance and execute commands
  echo "SSH into $INSTANCE_NAME..."
  gcloud compute ssh abalabanovic7818ri@"$INSTANCE_NAME" --zone=us-central1-a --quiet --command='
    sudo systemctl status docker
    sudo apt-get install -y docker.io
    sudo systemctl start docker

    # Check if container exists
    if sudo docker ps -q --filter "name=spring-petclinic" || sudo docker ps -aq --filter "name=spring-petclinic"; then
      echo "Container already exists."
      sudo docker stop spring-petclinic
      sudo docker rm spring-petclinic
      echo "Container removed!"
    else
      echo "Container not found!"
    fi

    # Pull latest image and run container
    sudo docker pull "'"$DOCKER_IMAGE_NAME"'"
    sudo docker run -d --name spring-petclinic \
      -e SPRING_PROFILES_ACTIVE=mysql \
      -e GOOGLE_APPLICATION_CREDENTIALS=/home/gcp-owner.json \
      -p 80:8080 \
      -v $(pwd)/gcp-owner.json:/home/gcp-owner.json \
      "'"$DOCKER_IMAGE_NAME"'"
  '

done
